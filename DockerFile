FROM python:3.6
RUN apt-get update
RUN mkdir /app
WORKDIR /app
COPY source/app.py /app/app.py
COPY source/requirements.txt /app/requirements.txt
RUN pip install -U -r /app/requirements.txt
RUN apt-get update && apt-get install vim -y
EXPOSE 80
CMD ["python","app.py"]